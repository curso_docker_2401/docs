# **todo** Project

Proyecto para practicar la tecnología Docker en un escenario real a través del desarrollo de una aplicación web para gestionar una lista de tareas.

## Diagrama

![img](./project_develop.png)

# Ejercicios Guiados

## 1. Crea un proyecto Git

Hacer **fork** del proyecto [todolist](https://gitlab.com/curso_docker_2401/todolist).

## 2. Creación de contenedor "builder"

```bash
$ docker build -t todo-builder .
$ docker images
REPOSITORY     TAG       IMAGE ID       CREATED          SIZE
todo-builder   latest    d5933c6363cf   16 minutes ago   162MB

```

## 3. Inialización del servicio todo-front

Desde la carpeta principal del proyecto `todolist`, ejecuta:

```bash
docker run -it --rm -v ${pwd}:/app todo-builder "npm create vue@latest"
```

El proyecto **todo-front** tendrá las siguientes opciones:
```bash
✔ Project name: … todo-front
✔ Add TypeScript? … No / Yes
✔ Add JSX Support? … No / Yes
✔ Add Vue Router for Single Page Application development? … No / Yes
✔ Add Pinia for state management? … No / Yes
✔ Add Vitest for Unit Testing? … No / Yes
✔ Add an End-to-End Testing Solution? › No
✔ Add ESLint for code quality? … No / Yes
✔ Add Vue DevTools extension for debugging? (experimental) … No / Yes
```

```bash
docker run -it --rm -v ${pwd}/todo-front:/app todo-builder "npm install"
```

Modifica el fichero `todo-front/package.json` añadiendo `--host` a la clave `dev` objeto `scripts`:

```php
  "scripts": {
    "dev": "vite --host",
    "build": "vite build",
    "preview": "vite preview"
  },
```

A continuación, desde la carpeta principal del proyecto `todolist`, ejecuta:

```bash
docker run -it --rm -p 5173:5173 -v ${pwd}/todo-front:/app todo-builder "npm run dev"
```

Abre un navegador en la dirección `http://localhost:5173` y comprueba que se visualiza la página principal de vue.

A continuación, modifica la línea 11 del fichero `todo-front/src/App.vue`:

```html
      <HelloWorld msg="Hello world!" />
```
Guarda el fichero y comprueba que en el navegador se puede ver el mensaje "Hello world!" sin recargar la página.

## 4. Inicialización servicio todo-api

Desde la carpeta principal del proyecto `todolist`, ejecuta:

```bash
docker run -it --rm -v ${pwd}:/app todo-builder "composer create-project --prefer-dist laravel/lumen todo-api"

docker run -it --rm -p 8000:8000 -v ${pwd}/todo-api:/app todo-builder "php -S 0.0.0.0:8000 -t public"
```
En el navegador abre la dirección `http://localhost:8000` y comprueba que aparece el mensaje de bienvenida de lumen:

Modifica la línea 17 del fichero `todo-api\routes\web.php`:

```bash
return "hello world";
```
Vuelve a recargar la página y comprueba que aparece "hello world".

## 5. Inicialización del servicio mariadb

```bash
docker run -d --name todo-mariadb --env MARIADB_ROOT_PASSWORD=password  mariadb:latest
```

Comprobamos que podemos acceder a la base de datos:

```bash
docker exec -it todo-mariadb bash
root@23faf32cbc45:/# mariadb todo -p
MariaDB [todo]> show tables;
Empty set (0.002 sec)
MariaDB [todo]> quit
Bye
root@23faf32cbc45:/# exit
exit
```

## 6. Desarrollo: Listar tareas

> :warining: Cierra todas las consolas anteriores.

### 6.1 Iniciando la base de datos

```bash
docker network create test_network

docker run -d --name todo-mariadb --env MARIADB_ROOT_PASSWORD=password --env MARIADB_DATABASE=todo --network=test_network  mariadb:latest
```

Comprobamos que la base de datos existe y el usuario root tiene acceso:

```bash
docker exec -it todo-mariadb bash
root@23faf32cbc45:/# mariadb todo -p
MariaDB [todo]> show tables;
Empty set (0.002 sec)
MariaDB [todo]> quit
Bye
root@23faf32cbc45:/# exit
exit
```

### 6.2 Modelo task en lumen

Modificamos el fichero `todo-api/.env`:

```bash
DB_CONNECTION=mysql
DB_HOST=todo-mariadb
DB_PORT=3306
DB_DATABASE=todo
DB_USERNAME=root
DB_PASSWORD=password
```

En el archivo `bootstrap/app.php` se necesita quitar los comentarios de las líneas `$app->withFacades();` y `$app->withEloquent();`

 Creamos la tabla `table_tasks`. Para ello, ejecutamos un contenedor con un `bash` como PID1:

```bash
docker run -it --rm -v ${pwd}/todo-api:/app --network=test_network todo-builder "bash"

2141d600deda:/app# php artisan make:migration table_tasks --create=tasks

  INFO  Migration [/app/database/migrations/2024_03_20_082624_table_tasks.php] created successfully.

```

Y modificamos el fichero `migrations/<fecha>_table_tasks.php`:

```php
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('name', 200);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tasks');
    }
};

```
Creamos el fichero `app\Models\TaskModel.php`;

```php
<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class TaskModel extends Model{

    protected $table = 'tasks';

}
```

Ejecuta:

```bash
2141d600deda:/app# php artisan migrate

   INFO  Preparing database.

  Creating migration table ................. 11ms DONE

   INFO  Running migrations.

  2024_03_20_084157_table_tasks ............ 6ms DONE

```

Comprueba que se han creado las tablas:

```bash
docker exec -it todo-mariadb bash
root@23faf32cbc45:/# mariadb todo -p
MariaDB [todo]> show tables;
+----------------+
| Tables_in_todo |
+----------------+
| migrations     |
| tasks          |
+----------------+
MariaDB [todo]> quit
Bye
root@23faf32cbc45:/# exit
exit
```

Ahora vamos a insertar algunos datos de ejemplo en la tabla tasks:

```
d5a7fb5ffe53:/app# php artisan make:seeder TaskSeeder

```
Modifica el fichero `Database\Seeders\TaskSeeder.php`:

```php
<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 1; $i <= 10; $i++) {
            DB::table('tasks')->insert([
                'name' => Str::random(10)
            ]);;
        }
    }
}

```
Y modifica el fichero `Database\Seeders\DatabaseSeeder.php`:

```php
<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            TaskSeeder::class
        ]);
    }
}
```


Ejecuta la siguiente instrucción para regenerar la base de datos e insertar datos:

```bash
d5a7fb5ffe53:/app# php artisan migrate:fresh --seed
  Dropping all tables ....................... 33ms DONE

   INFO  Preparing database.

  Creating migration table ................... 5ms DONE

   INFO  Running migrations.

  2024_03_20_084157_table_tasks .............. 3ms DONE


   INFO  Seeding database.

  Database\Seeders\TaskSeeder ................. RUNNING
  Database\Seeders\TaskSeeder ............... 3 ms DONE
```

Añade al fichero `routes/web.php` el siguiente código:

```php
$router->get('/tasks', function () use ($router) {
    $data = \App\Models\TaskModel::all();
    return response($data);
});
```


### 6.3 CORS

Vamos a configurar las CORS para permitir que nuestro frontend pueda consumir la API. Puedes leer más sobre las CORS [aqui](https://developer.mozilla.org/es/docs/Glossary/CORS).

Creamos un fichero `CorsMiddleware.php` en `app/Http/Middleware` con el siguiente contenido:

```php
<?php

namespace App\Http\Middleware;

use Closure;

class CorsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Allow all origins
        $headers = [
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS',
            'Access-Control-Allow-Headers' => 'Origin, Content-Type, X-Auth-Token, Authorization, Accept',
            'Access-Control-Allow-Credentials' => 'true',
        ];

        // Handle preflight OPTIONS requests
        if ($request->getMethod() === 'OPTIONS') {
            return response()->json('OK', 200, $headers);
        }

        // Add CORS headers to all responses
        $response = $next($request);
        foreach ($headers as $key => $value) {
            $response->header($key, $value);
        }

        return $response;
    }
}
```

Y habilitamos el middleware en `bootstrap/app.php`:

```php
$app->middleware([
    App\Http\Middleware\CorsMiddleware::class
]);
```

Iniciamos el servidor web:

```bash
docker run -it --rm --name todo-api -p 8000:8000 -v ${pwd}/todo-api:/app --network=test_network todo-builder "php -S 0.0.0.0:8000 -t public"
```

Abrimos un navegador en la URL `http://localhost:8000/tasks` y comprobamos que visualizamos las tareas.


## 6.4 Visualizando task en vue

Modifica el fichero `src\App.vue`:

```javascript
<template>
  <div>
    <h1>Tareas</h1>
    <ul>
    
      <li v-for="task in tasks" :key="task.id">
       {{ task.id}} - {{ task.name }}
      </li>
    </ul>
  </div>
</template>

<script>
export default {
  data() {
    return {
      tasks: []
    };
  },
  mounted() {
    this.fetchTasks();
  },
  methods: {
    async fetchTasks() {
      try {
        const response = await fetch('http://todo-api:8000/tasks');
        if (!response.ok) {
          throw new Error('Error al obtener las tareas');
        }
        this.tasks = await response.json();
      } catch (error) {
        console.error(error);
      }
    }
  }
};
</script>

```
Levantamos el servidor:

```bash
docker run -it --rm -p 5173:5173 --network=test_network -v ${pwd}/todo-front:/app todo-builder "npm run dev"
```

Accedemos a la URL: `http://localhost:5173/` y vemos que no se visualizan las tareas. Si inspecionamos la consola del navegador, vemos el siguiente error:

```http
GET http://todo-api:8000/tasks net::ERR_NAME_NOT_RESOLVED
```

¿Sabrías solucionar el problema?

# Ejercicios Propuestos

El siguiente bloque de ejercicios está diseñado para perfeccionar nuestras habilidades con Docker CLI. En la siguiente sesión veremos como simplificar el proceso de levantar los diferentes servicios para desarrollo usando `docker-compose`. 

## Orientados al desarrollo


1. Arrancar los 3 servicios haciendo que coincidan los puertos con los especificados en el diagrama.
1. Arrancar los 3 servicios y comprueba que desde todo-frontend puedes hacer ping a la base de datos (`ping todo-mariadb`). Modifica las diferentes instrucciones de `docker run` haciendo que coincidan las redes con las especificadas en el diagrama. Asegúrate que desde todo-front no puedes hacer: `ping todo-mariadb`.
1. La base de datos mariadb debe tener persistencia en un volumen. Lee el apartado **Where to Store Data** de la [documentación](https://hub.docker.com/_/mariadb). 
1. Añadir nuevas funcionalidades. Puedes desarrollar una funcionalidad que te resulte interesante o seleccionar alguna/as de las siguientes:
    - Visualizar datos de otro modelo. Por ejemplo, crear el modelo, migración y seeder de User y mostrarlo en vue.
    - Añadir CRUD sobre Tasks o sobre el modelo User.
    - Añadir un nuevo frontend con una tecnología distinta (por ejemplo, Angular) que consuma la API.
    - Añadir un nuevo backend con una tecnología distinta (por ejemplo, ExpressJS) que sea consumida por Vue.
    - Cambiar la base de datos (MySql, Mongo, etc.)


## Orientados a la administración de sistemas

1. Arrancar los 3 servicios haciendo que coincidan los puertos con los especificados en el diagrama.
1. Arrancar los 3 servicios y comprueba que desde todo-frontend puedes hacer ping a la base de datos (`ping todo-mariadb`). Modifica las diferentes instrucciones de `docker run` haciendo que coincidan las redes con las especificadas en el diagrama. Asegúrate que desde todo-front no puedes hacer: `ping todo-mariadb`.
1. La base de datos mariadb debe tener persistencia en un volumen. Lee el apartado **Where to Store Data** de la [documentación](https://hub.docker.com/_/mariadb). 
1. Guardar logs de Vue3, Lumen10. Inicia un contenedor alpine con dicho volumen y, mientras realizas peticiones, visualiza los logs haciendo uso de tail -f sobre el fichero que contenga los logs.

    - Puedes guardar los logs de los diferentes servicios:
      - vue: añadiendo `> vue_logs.txt` en la instrucción `npm run serve`. Puedes hacerlo permanente modificando el fichero `composer.json` o en la misma instrucción de docker run: `docker run -it [..] "npm run serve > vue_logs.txt`.
      - lumen: Modifica el docker run: `docker run -it [..] "php -S 0.0.0.0:8000 -t public > lumen_logs.txt`

1. ¡Listo para producción! Genera un contenedor con un servidor web (Apache, Nginx, ..) que contenga la carpeta `public` del proyecto **todo-api** y la carpeta `dist` que genera el comando `npm run build` para el proyecto **todo-front**. Inicia dicha imagen y comprueba que funciona correctamente.


# Soluciones a los ejercicios

### Creamos las redes

```bash
docker network create todo-frontend
docker network create todo-backend 
```

### todo-front

Modificamos el fichero `vite.config.ts` para usar el puerto 8080:

```js
import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  server: {
    port: 8080
  }
})
```

Instalamos dependencias y arrancamos el servidor:

```bash
docker run -it --rm -v $(pwd)/todo-front:/app todo-builder "npm install"
docker run -d \
    --name todo-front \
    -p 8080:8080 \
    -v $(pwd)/todo-front:/app \
    --network=todo-frontend \
    todo-builder \
    "npm run dev"
```
> Comandos útiles:
>
> `docker logs -f todo-front`
>
> `docker exec -it todo-front bash`

### todo-back

Instalamos dependencias y arrancamos el servidor:

```bash
docker run -it --rm -v $(pwd)/todo-api:/app todo-builder "composer install"
docker run -d  \
    --name todo-api \
    -p 8081:8081 \
    -v $(pwd)/todo-api:/app \
    --network=todo-frontend \
    --network=todo-backend \
    todo-builder \
    "php -S 0.0.0.0:8081 -t public"
```

> Comandos útiles:
>
> `docker logs -f todo-api`
>
> `docker exec -it todo-api bash`

### todo-mariadb

Creamos un volumen:

```bash
docker volume create todo-mariadb
```

```bash
docker run -d \
    --name todo-mariadb \
    -v todo-mariadb:/var/lib/mysql:Z \
    --env MARIADB_ROOT_PASSWORD=password \
    --network=todo-backend \
     mariadb:latest 
```


