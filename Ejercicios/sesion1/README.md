# Ejercicios propuestos

## 1. Crear una cuenta en Gitlab

https://gitlab.com/users/sign_up


## 2. Crear una cuenta en Docker Hub

https://hub.docker.com/signup

### Realizar login en Docker Hub en Docker CLI

```bash
$ docker login 
```

## 3. El "Hola Mundo" de docker

Vamos a comprobar que todo funciona creando nuestro primer contenedor desde la imagen `hello-world`:

```bash
$ docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
0e03bdcc26d7: Pull complete 
Digest:     sha256:31b9c7d48790f0d8c50ab433d9c3b7e17666d6993084c002c2ff1ca09b96391d
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working     correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs    the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which  sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

Pero, ¿qué es lo que está sucediendo al ejecutar esa orden?:

* Al ser la primera vez que ejecuto un contenedor basado en esa imagen, la imagen `hello-word` se descarga desde el repositorio que se encuentra en el registro que vayamos a utilizar, en nuestro caso DockerHub.
* Muestra el mensaje de bienvenida que es la consecuencia de ejecutar un comando al crear y arrancar un contenedor basado en esa imagen.

Si listamos los contenedores que se están ejecutando (`docker ps`):

```bash
$ docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
```
Comprobamos que este contenedor no se está ejecutando. **Un contenedor ejecuta un proceso y cuando termina la ejecución, el contenedor se para.**

Para ver los contenedores que no se están ejecutando:

```bash
$ docker ps -a
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                     PORTS               NAMES
372ca4634d53        hello-world         "/hello"            8 minutes ago       Exited (0) 8 minutes ago                       elastic_johnson
```

Para eliminar el contenedor podemos identificarlo con su `id`:

```bash
$ docker rm 372ca4634d53
```

o con su nombre:

```bash
$ docker rm elastic_johnson
```


## 4. Ejecución simple de contenedores

Con el comando `run` vamos a crear un contenedor donde vamos a ejecutar un comando, en este caso vamos a crear el contenedor a partir de una imagen ubuntu. Como todavía no hemos descargado ninguna imagen del registro docker hub, es necesario que se descargue la  imagen. Si la tenemos ya en nuestro ordenador no será necesario la descarga. 

```bash
$ docker run ubuntu echo 'Hello world' 
Unable to find image 'ubuntu:latest' locally
latest: Pulling from library/ubuntu
8387d9ff0016: Pull complete 
...
Status: Downloaded newer image for ubuntu:latest
Hello world
```

Comprobamos que el contenedor ha ejecutado el comando que hemos indicado y se ha parado:

```bash
$ docker ps -a
CONTAINER ID        IMAGE              COMMAND                  CREATED               STATUS                      PORTS               NAMES
3bbf39d0ec26        ubuntu              "echo 'Hello wo…"   31 seconds ago      Exited     (0) 29 seconds ago                       wizardly_edison
```

Con el comando `docker images` podemos visualizar las imágenes que ya tenemos descargadas en nuestro registro local:

```bash
$ docker images
REPOSITORY          TAG                 IMAGE ID           CREATED             SIZE
ubuntu              latest              f63181f19b2f        7 days ago          72.9MB
hello-world         latest              bf756fb1ae65        13 months ago       13.3kB
```


## 5. Ejecutando un contenedor interactivo

En este caso usamos la opción `-i` para abrir una sesión interactiva, `-t` nos permite crear un pseudo-terminal que nos va a permitir interaccionar con el contenedor, indicamos un nombre del contenedor con la opción `--name`, y la imagen que vamos a utilizar para crearlo, en este caso `ubuntu`,  y por último el comando que vamos a ejecutar, en este caso `bash`, que lanzará una sesión bash en el contenedor:

```bash
$  docker run -it --name contenedor1 ubuntu bash 
root@2bfa404bace0:/#
```

El contenedor se para cuando salimos de él. Para volver a conectarnos a él:

```bash
$ docker start contendor1
contendor1
$ docker attach contendor1
root@2bfa404bace0:/#
```

Si el contenedor se está ejecutando podemos ejecutar comandos en él con el subcomando `exec`:

```bash
$ docker start contendor1
contendor1
$ docker exec contenedor1 ls -al
```

Con la orden `docker restart` reiniciamos el contendor, lo paramos y lo iniciamos.

Para mostrar información de un contenedor ejecutamos `docker inspect`:

```bash
$ docker inspect contenedor1 
[
    {
        "Id": "178871769ac2fcbc1c73ce378066af01436b52a15894685b7321088468a25db7",
        "Created": "2021-01-28T19:12:21.764255155Z",
        "Path": "bash",
        "Args": [],
        "State": {
            "Status": "exited",
            "Running": false,
            "Paused": false,
            ...
```

Nos muestra mucha información, está en formato JSON (JavaScript Object Notation) y nos da datos sobre aspectos como:

* El id del contenedor.
* Los puertos abiertos y sus redirecciones
* Los *bind mounts* y volúmenes usados.
* El tamaño del contenedor
* La configuración de red del contenedor.
* El *ENTRYPOINT* que es lo que se ejecuta al hacer docker run.
* El valor de las variables de entorno.
* Y muchas más cosas....

En realidad, todas las imágenes tienen definidas un proceso que se ejecuta, en concreto la imagen `ubuntu` tiene definida por defecto el proceso `bash`, por lo que podríamos haber ejecutado:

```bash
$ docker run -it --name contenedor1 ubuntu
```

## 6. Creando un contenedor demonio

En esta ocasión hemos utilizado la opción `-d` del comando `run`, para que la ejecución del comando en el contenedor se haga en segundo plano.

```bash
$ docker run -d --name contenedor2 ubuntu bash -c "while true; do echo hello world; sleep 1; done"
7b6c3b1c0d650445b35a1107ac54610b65a03eda7e4b730ae33bf240982bba08
```

> NOTA: En la instrucción `docker run` hemos ejecutado el comando con `bash -c` que nos permite ejecutar uno o mas comandos en el contenedor de forma más compleja (por ejemplo, indicando ficheros dentro del contenedor).

* Comprueba que el contenedor se está ejecutando
* Comprueba lo que está haciendo el contenedor (`docker logs contenedor2`)

Por último podemos parar el contenedor y borrarlo con las siguientes instrucciones:

```bash
$ docker stop contenedor2
$ docker rm contenedor2
```

Hay que tener en cuenta que un contenedor que esta ejecutándose no puede ser eliminado. Tendríamos que para el contenedor y posteriormente borrarlo. Otra opción es borrarlo a la fuerza:

```bash
$ docker rm -f contenedor2
```


## 7. Configuración de contenedores con variables de entorno

Más adelante veremos que al crear un contenedor que necesita alguna configuración específica, lo que vamos a hacer es crear variables de entorno en el contenedor, para que el proceso que inicializa el contenedor pueda realizar dicha configuración.

Para crear una variable de entorno al crear un contenedor usamos el flag `-e` o `--env`:

```bash
$ docker run -it --name prueba -e USUARIO=prueba ubuntu bash
root@91e81200c633:/# echo $USUARIO
prueba
```

## 8. Configuración de un contenedor con la imagen MySQL

En ocasiones es obligatorio el inicializar alguna variable de entorno para que el contenedor pueda ser ejecutado. Si miramos la [documentación](https://hub.docker.com/_/mysql) en Docker Hub de la imagen MySQL, observamos que podemos definir algunas variables de entorno para la creación y configuración del contenedor (por ejemplo: `MYSQL_DATABASE`,`MYSQL_USER`, `MYSQL_PASSWORD`,...). Pero hay una que la tenemos que indicar de forma obligatoria, la contraseña del usuario `root` (`MYSQL_ROOT_PASSWORD`), por lo tanto:

```bash
$ docker run -d --name mysql -e MYSQL_ROOT_PASSWORD=secret mysql
$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED                STATUS              PORTS               NAMES
9c3effd891e3        mysql             "docker-entrypoint.s…"   8 seconds ago       Up 7   seconds        3306/tcp            mysql
```

Podemos ver que se ha creado una variable de entorno:

```bash
$ docker exec -it mysql env
...
MYSQL_ROOT_PASSWORD=secret
...
```

Y para acceder podemos ejecutar:

```bash
$ docker exec -it mysql bash                                  
root@9c3effd891e3:/# mysql -u root -p"$MYSQL_ROOT_PASSWORD" 
...

mysql [(none)]> 
```
Otra forma de hacerlo sería:

```bash
$ docker exec -it mysql mysql -u root -p
Enter password: 
...
mysql [(none)]> 
```

## 9. Contenedor Nginx a partir de contenedor existente

Vamos a crear un contenedor Nginx a partir de la imagen base de Nginx disponible en Docker Hub (https://hub.docker.com/_/nginx).

```bash
$ docker pull nginx
$ docker run -it --name nginx_v1 nginx bash
$ echo "<h1>Curso Docker </h1>" > /usr/share/nginx/html/index.html
$ exit
$ docker commit nginx_v1 my_nginx:v1
```

1. Comprueba que la imagen se ha creado correctamente.
2. Inicia un contenedor en modo interactivo a partir la imagen my_nginx:v1 y comprueba el contenido del fichero index.html.

## 10. Cambiando de nombre una imagen existente

Vamos a renombrar la imagen anterior. Para ello vamos a hacer uso del commando [docker image tag](https://docs.docker.com/reference/cli/docker/image/tag/).

```bash
$ docker image tag my_nginx:v1 my_nginx:v2
```

Comprueba que se ha creado una nueva imagen my_nginx:v2. Inicia el contenedor en modo interactivo y comprueba el contenido del fichero index.html.

A continuación, vamos a comprobar a nivel interno que cambios se han producido. Compara las salidas de los siguientes comandos: ¿Qué cambios observas? (nota: haz uso de dos PowerShell para comparar mejor los cambios o haz uso de un editor de textos como VSCode).

Ahora, vamos a volver a repetir los pasos del apartado anterior para crear una nueva imagen que contenga un fichero de texto `hola.txt` a partir de la imagen my_nginx:2

```bash
$ docker run -it --name nginx_v3 nginx:v2 bash
$ echo "Soy un nuevo fichero de texto" > hola.txt
$ cat hola.txt
Soy un nuevo fichero de texto
$ exit
$ docker commit nginx_v3 my_nginx:v3
```

Ejecuta ahora `docker inspect` sobre las imágenes `my_nginx:v1`, `my_nginx:v2` y `my_nginx:v3` y observa las diferencias. Deberías observar que my_nginx:v3 tiene una nueva capa en el filesystem.

En Docker, las imágenes se componen de múltiples capas que se apilan una encima de la otra para formar una imagen completa. Cada capa representa un cambio o una adición en el sistema de archivos del contenedor. Esto permite que Docker sea altamente eficiente en el almacenamiento y la distribución de imágenes, ya que las capas son reutilizables y se pueden compartir entre múltiples imágenes.

Organización de las capas de Docker:

1. **Base Image**: Cada imagen Docker comienza con una "imagen base". Esta imagen base proporciona el sistema de archivos inicial y puede ser una imagen básica de un sistema operativo como Ubuntu, CentOS, Alpine Linux, etc.

2. **Capas adicionales**: A medida que se realizan cambios en la imagen, Docker crea capas adicionales sobre la imagen base. En el caso anterior, añadir el fichero hola.txt ha generado una nueva capa.

3. **Operaciones de solo lectura**: Cada capa en una imagen Docker es de solo lectura. Esto significa que no se pueden modificar directamente una vez que se han creado. Si se necesita modificar un archivo en una capa, Docker crea una nueva capa que contiene la modificación y la coloca encima de las capas existentes. 

4. **Union File System (UnionFS)**: Docker utiliza un sistema de archivos de unión (UnionFS) para combinar las capas y presentarlas como un solo sistema de archivos coherente dentro del contenedor. UnionFS permite que varias capas se monten una encima de la otra sin modificar las capas originales. Esto mejora la eficiencia del almacenamiento y reduce el tiempo necesario para crear y distribuir imágenes.

5. **Cache de capas**: Docker utiliza un sistema de almacenamiento en caché para evitar la recreación de capas que no han cambiado. Si una capa ya existe en el sistema local, Docker la reutiliza en lugar de volver a descargarla o reconstruirla, lo que acelera significativamente los tiempos de construcción y despliegue de imágenes.

## 11. Subiendo la imagen nginx_v3 a DockerHub

Para subir una imagen a DockerHub, el nombre de esta imagen debe de tener el formato <usuario_docker_hub>/<nombre_imagen>:<version>. Por ejemplo, la imagen [svc-tasks](https://hub.docker.com/repository/docker/jpaniorte/svc-tasks/general) es una imagen que hemos preparado para futuras sesiones y está almacenada en el espacio público de jpaniorte.

Con lo aprendido en los apartados anteriores, cambia el nombre a la imagen my_nginx:3 para que cumpla el formato de DockerHub y finalmente, ejecuta: `docker push <imagen>`para subirla. Accede a la web de DockerHub y comprueba que tienes la imagen en tu espacio.

Ahora, cambia el nombre de la imagen my_nginx:2 para que cumpla el formato de DockerHub y ejecuta: `docker push <imagen>` para subirla. Observa como se han estructurado ambas imágenes.

Finalmente, elimina todas las imagenes de tu sistema. Puedes ejecutar `docker rmi -f ${docker images -aq}` (o  `docker rm -f ${docker ps -aq}` para eliminar todos los contenedores)para eliminarlas todas. A continuación, descarga de nuevo ambas imágenes y comprueba ejecutando un contenedor interactivo, que en un caso tenemos el fichero hola.txt y en el otro no.

# Material para la siguiente clase 

https://docs.docker.com/reference/dockerfile/